package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"
)

var port int64 = 0
var secret string = ""

func setupSecret() {
	if len(os.Args) < 3 {
		panic("[GLOBAL_STORAGE] access key or port has not provided!\n")
	}

	secret = os.Args[1]
	port, _ = strconv.ParseInt(os.Args[2], 10, 16)
}

func checkMethod(allowedMethod string, rw http.ResponseWriter, r *http.Request) bool {
	switch r.Method {
	case allowedMethod:
		return true
	case "OPTIONS":
		rw.WriteHeader(200)
		return false
	default:
		rw.WriteHeader(405)
		rw.Write([]byte(fmt.Sprintf("Method %s not allowed", r.Method)))
		return false
	}
}

func checkSecret(rw http.ResponseWriter, r *http.Request) bool {
	reqSecret := r.Header.Get("x-secret")
	if reqSecret != secret {
		rw.WriteHeader(403)
		return false
	}

	return true
}

func main() {
	rand.Seed(time.Now().UnixNano())
	setupSecret()
	storage := StorageFromFile("data.json")

	http.HandleFunc("/get", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Add("Access-Control-Allow-Origin", "*")
		rw.Header().Add("Access-Control-Allow-Headers", "x-secret")

		pass := checkMethod("GET", rw, r)
		if !pass {
			return
		}

		pass = checkSecret(rw, r)
		if !pass {
			return
		}

		key := r.URL.Query().Get("key")
		val, hasVal := storage.GetItem(key)

		var resBody struct {
			KeyExists bool   `json:"keyExists"`
			Value     string `json:"value"`
		}
		if !hasVal {
			resBody = struct {
				KeyExists bool   `json:"keyExists"`
				Value     string `json:"value"`
			}{
				KeyExists: false,
				Value:     "",
			}
		} else {
			resBody = struct {
				KeyExists bool   `json:"keyExists"`
				Value     string `json:"value"`
			}{
				KeyExists: true,
				Value:     val,
			}
		}

		rawResBody, _ := json.Marshal(resBody)

		rw.WriteHeader(200)
		rw.Header().Add("Content-Type", "application/json")
		rw.Write(rawResBody)
	})

	http.HandleFunc("/set", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Add("Access-Control-Allow-Origin", "*")
		rw.Header().Add("Access-Control-Allow-Headers", "x-secret")

		pass := checkMethod("POST", rw, r)
		if !pass {
			return
		}

		pass = checkSecret(rw, r)
		if !pass {
			return
		}

		rawReqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			rw.WriteHeader(500)
			rw.Write([]byte("Cannot read request body"))
			return
		}

		var reqBody struct {
			Key   string `json:"key"`
			Value string `json:"value"`
		}
		err = json.Unmarshal(rawReqBody, &reqBody)
		if err != nil {
			rw.WriteHeader(400)
			rw.Write([]byte("Invalid request body"))
			return
		}

		storage.SetItem(reqBody.Key, reqBody.Value)

		resBody := struct {
			Success bool `json:"success"`
		}{
			Success: true,
		}
		rawResBody, _ := json.Marshal(resBody)

		rw.WriteHeader(200)
		rw.Header().Add("Content-Type", "application/json")
		rw.Write(rawResBody)
	})

	http.HandleFunc("/remove", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Add("Access-Control-Allow-Origin", "*")
		rw.Header().Add("Access-Control-Allow-Headers", "x-secret")

		pass := checkMethod("POST", rw, r)
		if !pass {
			return
		}

		pass = checkSecret(rw, r)
		if !pass {
			return
		}

		rawReqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			rw.WriteHeader(500)
			rw.Write([]byte("Cannot read request body"))
			return
		}

		var reqBody struct {
			Key string `json:"key"`
		}
		err = json.Unmarshal(rawReqBody, &reqBody)
		if err != nil {
			rw.WriteHeader(400)
			rw.Write([]byte("Invalid request body"))
			return
		}

		storage.RemoveItem(reqBody.Key)

		resBody := struct {
			Success bool `json:"success"`
		}{
			Success: true,
		}
		rawResBody, _ := json.Marshal(resBody)

		rw.WriteHeader(200)
		rw.Header().Add("Content-Type", "application/json")
		rw.Write(rawResBody)
	})
	addr := fmt.Sprintf(":%d", port)
	log.Fatal(http.ListenAndServe(addr, nil))
}
