package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"sync"
)

type Storage struct {
	dict         map[string]string
	dictMutex    *sync.Mutex
	saveSignal   chan struct{}
	snapshotPath string
}

func StorageFromFile(path string) Storage {
	rawContent, err := ioutil.ReadFile(path)

	if err != nil {
		panic(fmt.Sprintf("[GLOBAL_STORAGE] Can't open file %s\n", path))
	}

	var dict map[string]string
	err = json.Unmarshal(rawContent, &dict)

	if err != nil {
		panic(fmt.Sprintf("[GLOBAL_STORAGE] Err while parsing file: %s\n", err.Error()))
	}

	res := Storage{
		dict:         dict,
		dictMutex:    &sync.Mutex{},
		saveSignal:   make(chan struct{}),
		snapshotPath: path,
	}

	res.startFlushing()
	return res
}

func (self *Storage) GetItem(key string) (string, bool) {
	self.dictMutex.Lock()
	defer self.dictMutex.Unlock()
	v, ok := self.dict[key]
	return v, ok
}

func (self *Storage) SetItem(key string, val string) {
	self.dictMutex.Lock()
	defer self.dictMutex.Unlock()
	self.dict[key] = val

	go func() {
		self.saveSignal <- struct{}{}
	}()
}

func (self *Storage) RemoveItem(key string) bool {
	self.dictMutex.Lock()
	defer self.dictMutex.Unlock()
	_, ok := self.dict[key]

	if !ok {
		return false
	}

	delete(self.dict, key)

	go func() {
		self.saveSignal <- struct{}{}
	}()

	return true
}

func (self *Storage) startFlushing() {
	go func() {
		for {
			<-self.saveSignal

			self.dictMutex.Lock()
			raw, err := json.Marshal(self.dict)
			self.dictMutex.Unlock()
			if err != nil {
				fmt.Printf("[GLOBAL_STORAGE] Flushing err => %s\n", err.Error())
				continue
			}

			err = ioutil.WriteFile(self.snapshotPath, raw, 0666)
			if err != nil {
				fmt.Printf("[GLOBAL_STORAGE] Flushing file err => %s\n", err.Error())
				continue
			}
		}
	}()
}
